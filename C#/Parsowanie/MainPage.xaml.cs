﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.ComponentModel; //dla INotifyPropertyChanged
using HtmlAgilityPack;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Parsowanie
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        System.Net.Http.HttpClient httpClient;
        public event PropertyChangedEventHandler PropertyChanged;
        private string text;
        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                //odświeżenie zawartości kontrolki do której przypisana jest właściwość
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Text"));
            }
        }

        public MainPage()
        {
            this.InitializeComponent();
            //inicializacja httpClient
            httpClient = new System.Net.Http.HttpClient();
            DownloadDataFromSite();

        }
        private async void DownloadDataFromSite()
        {
            //treść
            try
            {
                string temp = await httpClient.GetStringAsync("http://flaw.hostingasp.pl/parsowanie/index.html");
                //parsowanie
                HtmlDocument htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(temp);
                HtmlNode htmlNode = htmlDocument.GetElementbyId("text");
                string tempText = string.Empty;
                List<HtmlNode> nodes = (from g in htmlNode.ChildNodes where g.OriginalName != "#comment" select g).ToList<HtmlNode>();
                foreach (HtmlNode tempNode in nodes)
                    tempText += tempNode.InnerText;
                Text = tempText;
            }
            catch
            {
                //obsluga błędu
            }
        }


        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
    }
}
